const { DB } = process.env;

const config = {
    development : {
			DB,
		},
		test: {

		},
		production: {

		}
};

const getConfig = (env) => {
	return config[env];
};

module.exports = getConfig;
