const router = require('express').Router();
const Teacher = require('../../models/mongo_mappers/teacher');

const getTeachers = (req, res) => {
	Teacher
		.find()
		.then((teachers) => {
			res.send({
				teachers,
			});
		})
		.catch((err) => {
			res.status(500).send({
				error: err.message || 'Error retrieving teachers',
			});
		});
};

router.get('/', getTeachers);
module.exports = router;
