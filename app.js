const app = require('express')();
require('dotenv').config();
const { PORT, NODE_ENV } = process.env;
const config = require('./config')(NODE_ENV);
const mongoose = require('mongoose');
app.use('/api', require('./app/controllers'));

app.listen(PORT, (err) => {
    if (!err) {
        mongoose.Promise = global.Promise;
        mongoose.connect(config.DB, { useNewUrlParser: true });
        console.log('server is running on port', PORT, ' in mode', NODE_ENV);
    } else {
        console.log('Error =>', err.message);
    }
});